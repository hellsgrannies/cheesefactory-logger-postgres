# cheesefactory-logger-postgres

-----------------

##### An interface for logging to a Postgres database.
[![PyPI Latest Release](https://img.shields.io/pypi/v/cheesefactory-logger-postgres.svg)](https://pypi.org/project/cheesefactory-logger-postgres/)
[![PyPI status](https://img.shields.io/pypi/status/cheesefactory-logger-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-logger-postgres/)
[![PyPI download month](https://img.shields.io/pypi/dm/cheesefactory-logger-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-logger-postgres/)
[![PyPI download week](https://img.shields.io/pypi/dw/cheesefactory-logger-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-logger-postgres/)
[![PyPI download day](https://img.shields.io/pypi/dd/cheesefactory-logger-postgres.svg)](https://pypi.python.org/pypi/cheesefactory-logger-postgres/)

### Main Features

* Log to a PostgreSQL database
* Table fields are customizable. 
* Query the log table.


#### Note: This package is still in beta status. As such, future versions may not be backwards compatible and features may change.


## Recent Changes
None


## Installation
The source is hosted at https://bitbucket.org/hellsgrannies/cheesefactory-logger-postgres

```sh
pip install cheesefactory-logger-postgres
```

## Dependencies

None

### Basic Usage

Coming Soon

#### Note: This package is still in beta status. As such, future versions may not be backwards compatible and features may change.
