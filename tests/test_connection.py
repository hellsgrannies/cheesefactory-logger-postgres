# test_connection.py

import logging
import socket

import pytest
from cheesefactory_logger_postgres.connection import CfLogPostgresConnection

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


#
# Test config
#

DB_HOST = 'cf_logger_postgres_postgres'  # Container hostname in docker-compose.yml
DB_PORT = 5432
DB_DB = 'postgres'
DB_USER = 'postgres'
DB_PASSWORD = 'testingtom'


def test_sftp_port_open():
    # Is port 5432 open on the server? It should be.
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = my_socket.connect_ex((DB_HOST, DB_PORT))
    assert result == 0, f'Postgres server ({DB_HOST}) not listening on port {str(DB_PORT)}'


@pytest.fixture(scope='module')
def db():
    postgres = CfLogPostgresConnection()
    postgres._connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_DB,
        user=DB_USER,
        password=DB_PASSWORD
    )
    yield postgres

    logger.debug('Teardown CfLogPostgresConnection instance.')
    postgres.close()


def test_cflogsqliteconnection():
    """Are attributes at expected values?"""
    connection = CfLogPostgresConnection()
    assert connection._connection is None
    assert connection._cursor is None
    assert connection.host is None
    assert connection.port is None
    assert connection.username is None
    assert connection.password is None
    assert connection.database is None
    assert connection.create_table is None


#
# PROPERTIES
#

def test_status(db):
    # Does the connection status() return what is expected? It should.
    status = db.status
    assert status == (True, 'ready')


def test_status_no_connection():
    """Do we get an expected status when no connection is present?"""
    connection = CfLogPostgresConnection()
    assert (False, 'No connection detected.') == connection.status


#
# PUBLIC METHODS
#

def test_close():
    """Does the connection close? NOPE. PROBLEM WITH CLOSE()"""
    # Make sure the file exists
    postgres = CfLogPostgresConnection()
    postgres._connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_DB,
        user=DB_USER,
        password=DB_PASSWORD
    )
    assert postgres.status == (True, 'ready')
    postgres.close()
    # assert postgres.status == (False, 'ready')


def test_execute(db):
    """Can the connection be used to execute queries?"""
    # Make sure the file exists
    sql = 'SELECT * from pg_catalog.pg_user'
    result = db.execute(sql)
    assert result[0] == ('postgres', 10, True, True, True, True, '********', None, None)
