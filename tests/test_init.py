# test_transfer.py

import pytest
import logging
import socket
from cheesefactory_logger_postgres import CfLogPostgres

logger = logging.getLogger(__name__)


#
# Test config
#

DB_HOST = 'cf_logger_postgres_postgres'  # Container hostname in docker-compose.yml
DB_PORT = 5432
DB_DB = 'postgres'
DB_USER = 'postgres'
DB_PASSWORD = 'testingtom'

SCHEMA = 'public'
TABLE = 'log_init'
FIELD_LIST = {
    'id': 'INT',
    'action': 'action_type',
    'action_ok': 'BOOLEAN',
    'client': 'TEXT',
    'local_host': 'TEXT',
    'local_path': 'TEXT',
    'notes': 'TEXT',
    'preserve_mtime': 'BOOLEAN',
    'preserve_mtime_ok': 'BOOLEAN',
    'redo': 'BOOLEAN',
    'remote_host': 'TEXT',
    'remote_path': 'TEXT',
    'remove_source': 'BOOLEAN',
    'remove_source_ok': 'BOOLEAN',
    'size': 'INT',
    'size_match_ok': 'BOOLEAN',
    'status': 'status_type',
    'suffix': 'TEXT',
    'suffix_ok': 'BOOLEAN',
    'datetime': 'TIMESTAMP',
}


def test_sftp_port_open():
    """Is port 5432 open on the server? It should be."""
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = my_socket.connect_ex((DB_HOST, DB_PORT))
    assert result == 0, f'SFTP server ({DB_HOST}) not listening on port {str(DB_PORT)}'


def create_table():
    log = CfLogPostgres()
    log._connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_DB,
        user=DB_USER,
        password=DB_PASSWORD
    )
    log.execute("CREATE TYPE action_type AS ENUM ('GET', 'PUT');")
    log.execute("CREATE TYPE status_type AS ENUM ('STARTED', 'OK', 'ERROR')")
    log.execute(f'''
        CREATE TABLE IF NOT EXISTS {SCHEMA}.{TABLE} (
            id SERIAL PRIMARY KEY NOT NULL,
            action action_type,
            action_ok BOOLEAN,
            client TEXT,
            local_host TEXT,
            local_path TEXT,
            notes TEXT,
            preserve_mtime BOOLEAN,
            preserve_mtime_ok BOOLEAN,
            redo BOOLEAN,
            remote_host TEXT,
            remote_path TEXT,
            remove_source BOOLEAN,
            remove_source_ok BOOLEAN,
            size INT,
            size_match_ok BOOLEAN,
            status status_type,
            suffix TEXT,
            suffix_ok BOOLEAN,
            datetime TIMESTAMP DEFAULT now()
        )
    ''')
    log.close()


def drop_table():
    log = CfLogPostgres()
    log._connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_DB,
        user=DB_USER,
        password=DB_PASSWORD
    )
    log.execute(f'DROP TABLE {SCHEMA}.{TABLE}')
    log.execute('DROP TYPE action_type')
    log.execute('DROP TYPE status_type')
    log.close()


def test_setup_and_teardown():
    try:
        logger.debug(f'Dropping table: {SCHEMA}.{TABLE}')
        drop_table()
    except:
        logger.debug(f'Table does not exist ({SCHEMA}.{TABLE}). Continuing.')
        pass

    logger.debug(f'Creating table {SCHEMA}.{TABLE}')
    create_table()

    # Make sure the table is created as expected.
    log = CfLogPostgres()
    log._connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_DB,
        user=DB_USER,
        password=DB_PASSWORD
    )
    log.schema = SCHEMA
    log.table = TABLE
    results = log.get_fields()
    assert str(results) == "[('log_init', 'id', 'integer'), ('log_init', 'action', 'USER-DEFINED'), " \
                           "('log_init', 'action_ok', 'boolean'), ('log_init', 'client', 'text'), " \
                           "('log_init', 'local_host', 'text'), ('log_init', 'local_path', 'text'), " \
                           "('log_init', 'notes', 'text'), ('log_init', 'preserve_mtime', 'boolean'), " \
                           "('log_init', 'preserve_mtime_ok', 'boolean'), ('log_init', 'redo', 'boolean'), " \
                           "('log_init', 'remote_host', 'text'), ('log_init', 'remote_path', 'text'), " \
                           "('log_init', 'remove_source', 'boolean'), ('log_init', 'remove_source_ok', 'boolean'), " \
                           "('log_init', 'size', 'integer'), ('log_init', 'size_match_ok', 'boolean'), " \
                           "('log_init', 'status', 'USER-DEFINED'), ('log_init', 'suffix', 'text'), " \
                           "('log_init', 'suffix_ok', 'boolean'), " \
                           "('log_init', 'datetime', 'timestamp without time zone')]"

    logger.debug(f'Dropping table {SCHEMA}.{TABLE}')
    drop_table()
    assert log.table_exists() is False


def test_cflogsqlite():
    """Are the defaults what we expect them to be?"""
    log = CfLogPostgres()
    assert log._connection is None
    assert log._cursor is None
    assert log.host is None
    assert log.port is None
    assert log.username is None
    assert log.password is None
    assert log.database is None
    assert log.create_table is None
    assert log.schema == 'public'
    assert log.table == 'log'
    assert log.field_list == {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'message': 'TEXT',
        'timestamp': 'TEXT DEFAULT CURRENT_TIMESTAMP',
    }


#
# CLASS METHODS
#

def test_connect(caplog):
    try:
        logger.debug(f'Dropping table: {SCHEMA}.{TABLE}')
        drop_table()
    except:
        logger.debug(f'Table does not exist ({SCHEMA}.{TABLE}). Continuing.')
        pass

    caplog.set_level(logging.DEBUG)
    logger.debug(f'Creating table {SCHEMA}.{TABLE}')
    create_table()  # Make the table to test against

    log = CfLogPostgres.connect(host=DB_HOST, port=DB_PORT, database=DB_DB, user=DB_USER, password=DB_PASSWORD,
                                schema=SCHEMA, table=TABLE, field_list=FIELD_LIST)
    assert f'Connected to log database: {log.database}.{log.schema}.{log.table}' in caplog.records[-1].msg

    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp', preserve_mtime=True,
        remote_host='172.16.1.1', remote_path='/upload', remove_source=True, status='STARTED'
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=True, remove_source_ok=True, size=2232, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status='OK'
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp5', preserve_mtime=True,
        remote_host='172.16.1.1', remote_path='/upload5', remove_source=True, status='STARTED'
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=False, remove_source_ok=True, size=245, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status='OK'
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp4', preserve_mtime=False,
        remote_host='172.16.1.1', remote_path='/upload4', remove_source=True, status='STARTED'
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=True, remove_source_ok=False, size=274, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status='ERROR'
    )

    # Read and test to make sure things are good so far. The last field is the timestamp that cannot be matched, because
    # it changes every run. So, it is removed with [:1] for the comparison.
    results = log.read_records()
    assert results[0][:-1] == (1, 'GET', None, 'CfTester', '192.168.1.1', '/tmp', 'done', True, True, None,
                               '172.16.1.1', '/upload', True, True, 2232, None, 'OK', None, None)
    assert results[1][:-1] == (2, 'GET', None, 'CfTester', '192.168.1.1', '/tmp5', 'done', True, False, None,
                               '172.16.1.1', '/upload5', True, True, 245, None, 'OK', None, None)
    assert results[2][:-1] == (3, 'GET', None, 'CfTester', '192.168.1.1', '/tmp4', 'done', False, True, None,
                               '172.16.1.1', '/upload4', True, False, 274, None, 'ERROR', None, None)

    # Close and reconnect to make sure it continues to append
    log.close()
    log = CfLogPostgres.connect(host=DB_HOST, port=DB_PORT, database=DB_DB, user=DB_USER, password=DB_PASSWORD,
                                schema=SCHEMA, table=TABLE, field_list=FIELD_LIST)
    assert f'Connected to log database: {log.database}.{log.schema}.{log.table}' in caplog.records[-1].msg

    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp3', preserve_mtime=True,
        remote_host='172.16.1.1', remote_path='/upload3', remove_source=True, status='ERROR'
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=True, remove_source_ok=True, size=780567, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status='OK'
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp2', preserve_mtime=True,
        remote_host='172.16.1.1', remote_path='/upload2', remove_source=False, status='ERROR'
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=True, remove_source_ok=True, size=35834562, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status='ERROR'
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp1', preserve_mtime=False,
        remote_host='172.16.1.1', remote_path='/upload1', remove_source=True, status='OK'
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=False, remove_source_ok=True, size=34535, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status='OK'
    )

    results = log.read_records()
    assert results[0][:-1] == (1, 'GET', None, 'CfTester', '192.168.1.1', '/tmp', 'done', True, True, None,
                               '172.16.1.1', '/upload', True, True, 2232, None, 'OK', None, None)
    assert results[1][:-1] == (2, 'GET', None, 'CfTester', '192.168.1.1', '/tmp5', 'done', True, False, None,
                               '172.16.1.1', '/upload5', True, True, 245, None, 'OK', None, None)
    assert results[2][:-1] == (3, 'GET', None, 'CfTester', '192.168.1.1', '/tmp4', 'done', False, True, None,
                               '172.16.1.1', '/upload4', True, False, 274, None, 'ERROR', None, None)
    assert results[3][:-1] == (4, 'GET', None, 'CfTester', '192.168.1.1', '/tmp3', 'done', True, True, None,
                               '172.16.1.1', '/upload3', True, True, 780567, None, 'OK', None, None)
    assert results[4][:-1] == (5, 'GET', None, 'CfTester', '192.168.1.1', '/tmp2', 'done', True, True, None,
                               '172.16.1.1', '/upload2', False, True, 35834562, None, 'ERROR', None, None)
    assert results[5][:-1] == (6, 'GET', None, 'CfTester', '192.168.1.1', '/tmp1', 'done', False, False, None,
                               '172.16.1.1', '/upload1', True, True, 34535, None, 'OK', None, None)

    # Clean up
    logger.debug(f'Dropping table {SCHEMA}.{TABLE}')
    drop_table()
    assert log.table_exists() is False


def test_connect_error_nofieldlist():
    try:
        logger.debug(f'Dropping table: {SCHEMA}.{TABLE}')
        drop_table()
    except:
        logger.debug(f'Table does not exist ({SCHEMA}.{TABLE}). Continuing.')
        pass

    create_table()
    with pytest.raises(ValueError) as excinfo:
        CfLogPostgres.connect(host=DB_HOST, port=DB_PORT, database=DB_DB, user=DB_USER, password=DB_PASSWORD,
                              schema=SCHEMA, table=TABLE)
    assert str(excinfo.value) == 'Missing field_list'
    drop_table()
