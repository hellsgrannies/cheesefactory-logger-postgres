# test_model.py

import logging
import socket
from time import sleep

import pytest
from cheesefactory_logger_postgres.exceptions import MissingFieldsError, MissingTableError, TablePrimaryKeyError
from cheesefactory_logger_postgres.model import CfLogPostgresModel

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


#
# Test config
#

DB_HOST = 'cf_logger_postgres_postgres'  # Container hostname in docker-compose.yml
DB_PORT = 5432
DB_DB = 'postgres'
DB_USER = 'postgres'
DB_PASSWORD = 'testingtom'

SCHEMA = 'public'
TABLE = 'log'
FIELD_LIST = {
    'id': 'SERIAL PRIMARY KEY',
    'action': 'TEXT',
    'action_ok': 'BOOLEAN',
    'client': 'TEXT',
    'local_host': 'TEXT',
    'local_path': 'TEXT',
    'notes': 'TEXT',
    'preserve_mtime': 'BOOLEAN',
    'preserve_mtime_ok': 'BOOLEAN',
    'redo': 'INTEGER',
}


def clean(db, schema: str = SCHEMA, table: str = TABLE) -> bool:
    """Drop a table.

    Returns:
        True, if successful. False, if not.
    """
    db.execute(f'DROP TABLE IF EXISTS {schema}.{table};')
    results = db.execute(
        f"SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname = '{schema}' AND tablename = '{table}';"
    )
    if len(results) == 0:
        return True
    else:
        return False


def test_sftp_port_open():
    """Is port 5432 open on the server? It should be."""
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = my_socket.connect_ex((DB_HOST, DB_PORT))
    assert result == 0, f'SFTP server ({DB_HOST}) not listening on port {str(DB_PORT)}'


@pytest.fixture(scope='module')
def db():
    postgres = CfLogPostgresModel()
    postgres._connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_DB,
        user=DB_USER,
        password=DB_PASSWORD
    )
    postgres.schema = SCHEMA
    postgres.table = TABLE
    postgres.field_list = FIELD_LIST
    yield postgres

    logger.debug('Teardown CfLogPostgresConnection instance.')
    postgres.close()


def test_execute(db):
    """Can the connection be used to execute queries?"""
    # Make sure the file exists
    sql = 'SELECT * from pg_catalog.pg_user'
    result = db.execute(sql)
    assert result[0] == ('postgres', 10, True, True, True, True, '********', None, None)


def test_cflogsqliteconnection():
    """Are attributes at expected values?"""
    model = CfLogPostgresModel()
    assert model._connection is None
    assert model._cursor is None
    assert model.host is None
    assert model.port is None
    assert model.username is None
    assert model.password is None
    assert model.database is None
    assert model.create_table is None
    assert model.schema == 'public'
    assert model.table == 'log'
    assert model.field_list == {}


#
# PROPERTIES
#

def test_pk_fields(db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db._create_table()  # With pre-defined FIELD_LIST in fixture
    assert db.pk_field == 'id'
    assert clean(db) is True

    db.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT PRIMARY KEY',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    db._create_table()
    assert db.pk_field == 'local_host'
    assert clean(db) is True

    db.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    db._create_table()
    assert db.pk_field == ''
    assert clean(db) is True


#
# PROTECTED METHODS
#

def test_create_table(db):
    """Does a table get properly created?"""
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db.field_list = FIELD_LIST  # Needed because running pytest on the entire file will pull field_list from last test.
    db._create_table()
    result = db.get_fields()

    assert result == [
        (TABLE, 'id', 'integer'),
        (TABLE, 'action', 'text'),
        (TABLE, 'action_ok', 'boolean'),
        (TABLE, 'client', 'text'),
        (TABLE, 'local_host', 'text'),
        (TABLE, 'local_path', 'text'),
        (TABLE, 'notes', 'text'),
        (TABLE, 'preserve_mtime', 'boolean'),
        (TABLE, 'preserve_mtime_ok', 'boolean'),
        (TABLE, 'redo', 'integer')
    ]
    assert clean(db) is True


def test_create_table_noconnection():
    """Will an error raise if no connection exists when creating table?"""
    model = CfLogPostgresModel()
    model.field_list = FIELD_LIST
    with pytest.raises(AttributeError) as excinfo:
        model._create_table()
    assert 'No Postgres connection exists. Connect before creating table.' in str(excinfo.value)


def test_create_table_nopk(caplog, db):
    """Does a table get properly created?"""
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    caplog.set_level(logging.DEBUG)

    db.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    db._create_table()
    result = db.get_fields()

    assert result == [
        (TABLE, 'id', 'integer'),
        (TABLE, 'action', 'text'),
        (TABLE, 'action_ok', 'integer'),
        (TABLE, 'client', 'text'),
        (TABLE, 'local_host', 'text'),
        (TABLE, 'local_path', 'text'),
        (TABLE, 'notes', 'text'),
        (TABLE, 'preserve_mtime', 'integer'),
        (TABLE, 'preserve_mtime_ok', 'integer'),
        (TABLE, 'redo', 'integer')
    ]
    assert caplog.records[-1].msg == f'Table ({SCHEMA}.{TABLE}) has no primary key. UPDATEs are not possible.'
    assert db.pk_field == ''
    assert clean(db) is True


def test_test_database(caplog, db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    caplog.set_level(logging.DEBUG)
    db._create_table()
    db._test_database()
    assert caplog.records[-1].msg == f'Table successfully tested: {db.schema}.{db.table}'
    assert clean(db) is True


def test_test_database_missing_fields(db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db._create_table()

    # Change the field list so a MissingFieldsError is raised.
    db.field_list = {
        'id': 'SERIAL PRIMARY KEY',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'new_field_1': 'BOOLEAN',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'new_field_2': 'INTEGER',
    }

    with pytest.raises(MissingFieldsError) as excinfo:
        db._test_database()
    assert str(excinfo.value) == f'Fields missing: ' \
                                 f'Table {db.schema}.{db.table} is missing required fields: new_field_1, new_field_2'
    assert clean(db) is True


def test_test_database_missingtable(db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    with pytest.raises(MissingTableError) as excinfo:
        db._test_database()
    assert str(excinfo.value) == f'Table ({TABLE}) missing in database ({db.schema}.{db.table})'


#
# PUBLIC METHODS
#

def test_exists(db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db.field_list = {
        'id': 'SERIAL PRIMARY KEY',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    db._create_table()

    db.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here.txt')
    db.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here2.txt')
    db.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here3.txt')
    db.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here3.txt')
    db.write_kwargs(action='GET', action_ok=1, client='client3', local_host='192.10.70.4',
                    local_path='/tmp/here3.txt')
    db.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.60.4',
                    local_path='/tmp/here5.txt')
    db.write_kwargs(action='GET', action_ok=0, client='client4', local_host='192.10.50.4',
                    local_path='/tmp/here4.txt')

    result = db.read_records()

    assert result == [
        (1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt'),
        (2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt'),
        (3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (5, 'GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt'),
        (6, 'PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt'),
        (7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt')
    ]

    exists = db.exists()
    assert exists is True

    exists = db.exists(where='action_ok = 0')
    assert exists is True

    exists = db.exists(where="local_host LIKE '192.10.60%'")
    assert exists is True

    exists = db.exists(where="local_host = 'nonexistent'")
    assert exists is False

    assert clean(db) is True


def test_list_fields(db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db.field_list = FIELD_LIST  # Needed because running pytest on the entire file will pull field_list from last test.
    db._create_table()
    fields = db.get_fields()
    assert str(fields) == f"[('{TABLE}', 'id', 'integer'), ('{TABLE}', 'action', 'text'), " \
                          f"('{TABLE}', 'action_ok', 'boolean'), ('{TABLE}', 'client', 'text'), " \
                          f"('{TABLE}', 'local_host', 'text'), ('{TABLE}', 'local_path', 'text'), " \
                          f"('{TABLE}', 'notes', 'text'), ('{TABLE}', 'preserve_mtime', 'boolean'), " \
                          f"('{TABLE}', 'preserve_mtime_ok', 'boolean'), ('{TABLE}', 'redo', 'integer')]"
    assert clean(db) is True


def test_read_records(db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db.field_list = {
        'id': 'SERIAL PRIMARY KEY',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    db._create_table()

    db.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here.txt')
    db.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here2.txt')
    db.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here3.txt')
    db.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                    local_path='/tmp/here3.txt')
    db.write_kwargs(action='GET', action_ok=1, client='client3', local_host='192.10.70.4',
                    local_path='/tmp/here3.txt')
    db.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.60.4',
                    local_path='/tmp/here5.txt')
    db.write_kwargs(action='GET', action_ok=0, client='client4', local_host='192.10.50.4',
                    local_path='/tmp/here4.txt')

    result = db.read_records()

    assert result == [
        (1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt'),
        (2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt'),
        (3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (5, 'GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt'),
        (6, 'PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt'),
        (7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt')
    ]
    assert clean(db) is True


def test_table_exists(db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db._create_table()
    assert db.table_exists() is True

    assert clean(db) is True
    assert db.table_exists() is False


def test_write_kwargs_insert(caplog, db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    caplog.set_level(logging.DEBUG)

    db.field_list = {
        'id': 'SERIAL PRIMARY KEY',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    db._create_table()

    pk = db.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                         local_path='/tmp/here.txt')
    assert caplog.records[-1].msg == \
           f"write(): INSERT INTO {db.schema}.{db.table} (action, action_ok, client, local_host, local_path) " \
           f"VALUES ('GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt') RETURNING id;"
    assert pk == 1

    pk = db.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                         local_path='/tmp/here2.txt')
    assert caplog.records[-1].msg == \
           f"write(): INSERT INTO {db.schema}.{db.table} (action, action_ok, client, local_host, local_path) " \
           f"VALUES ('PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt') RETURNING id;"
    assert pk == 2

    pk = db.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                         local_path='/tmp/here3.txt')
    assert caplog.records[-1].msg == \
           f"write(): INSERT INTO {db.schema}.{db.table} (action, action_ok, client, local_host, local_path) " \
           f"VALUES ('GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt') RETURNING id;"
    assert pk == 3

    result = db.execute(f'SELECT * from {db.schema}.{db.table};')
    assert result == [
        (1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt'),
        (2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt'),
        (3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt')
    ]
    assert clean(db) is True


def test_write_kwargs_invalidkwargs(db):
    """Is an error raised if kwargs have invalid keys?"""
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    db.field_list = {
        'id': 'SERIAL PRIMARY KEY',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    db._create_table()
    with pytest.raises(ValueError) as excinfo:
        db.write_kwargs(bad_key='this is not good.')
    assert str(excinfo.value) == f'Invalid key(s): bad_key. Acceptable values are id, action, action_ok, client, ' \
                                 f'local_host, local_path'


def test_write_kwargs_missingkwargs():
    """Is an error raised if missing kwargs?"""
    model = CfLogPostgresModel()
    with pytest.raises(ValueError) as excinfo:
        model.write_kwargs()
    assert str(excinfo.value) == f'Missing kwargs. Cannot write to database.'


def test_write_kwargs_update(caplog, db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    caplog.set_level(logging.DEBUG)
    db.field_list = {
        'id': 'SERIAL PRIMARY KEY',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    db._create_table()

    pk = db.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                         local_path='/tmp/here.txt')
    assert caplog.records[-1].msg == \
           f"write(): INSERT INTO {db.schema}.{db.table} (action, action_ok, client, local_host, local_path) " \
           f"VALUES ('GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt') RETURNING id;"
    assert pk == 1

    pk = db.write_kwargs(pk=1, action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                         local_path='/tmp/here2.txt')
    assert caplog.records[-2].msg == \
           f"write(): UPDATE {db.schema}.{db.table} SET action = 'PUT', action_ok = 1, client = 'test run', " \
           f"local_host = '192.10.10.4', local_path = '/tmp/here2.txt' WHERE id = 1;"
    assert pk == 1

    pk = db.write_kwargs(pk=1, action_ok=1, client='bad bob', local_path='/tmp/here32.txt')
    assert caplog.records[-2].msg == \
           f"write(): UPDATE {db.schema}.{db.table} SET action_ok = 1, client = 'bad bob', " \
           f"local_path = '/tmp/here32.txt' WHERE id = 1;"
    assert pk == 1

    pk = db.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                         local_path='/tmp/here3.txt')
    assert caplog.records[-1].msg == \
           f"write(): INSERT INTO {db.schema}.{db.table} (action, action_ok, client, local_host, local_path) " \
           f"VALUES ('GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt') RETURNING id;"
    assert pk == 2

    result = db.execute(f'SELECT * from {db.schema}.{db.table};')
    assert result == [
        (1, 'PUT', 1, 'bad bob', '192.10.10.4', '/tmp/here32.txt'),
        (2, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt')
    ]
    assert clean(db) is True


def test_write_kwargs_update_nopk(caplog, db):
    assert db.status == (True, 'ready') or db.status == (True, 'begin')
    assert clean(db) is True

    caplog.set_level(logging.DEBUG)
    db.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    db._create_table()
    assert caplog.records[-1].msg == f'Table ({db.schema}.{db.table}) has no primary key. UPDATEs are not possible.'

    pk = db.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                         local_path='/tmp/here.txt')
    assert caplog.records[-2].msg == \
           f"write(): INSERT INTO {db.schema}.{db.table} (action, action_ok, client, local_host, local_path) " \
           f"VALUES ('GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt')"
    assert pk is None

    with pytest.raises(TablePrimaryKeyError) as excinfo:
        db.write_kwargs(pk=1, action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                        local_path='/tmp/here2.txt')
    assert str(excinfo.value) == \
           f'Table {db.database}.{db.schema}.{db.table} Error: The table has no primary key. SQL UPDATE cannot happen.'
    assert clean(db) is True
